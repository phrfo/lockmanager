#ifndef ITEM_H
#define ITEM_H

#include "lock.h"
#include "general.h"
#include "waitq.h"

class Item
{
private:
    string id;
    Lock lock;
    WaitQ queue;
public:
    Item(string);

    string getId();
    Lock getLock();

    bool isLocked();
    bool isLockedForWrite(Transaction);
    bool isLockedForRead();

    void addWriteLock(Transaction);
    void addReadLock(Transaction);
    void unlock(Transaction);

    bool isWaitQEmpty();
    void addInWaitQ(Transaction, int);
    void removeFromQueue(Transaction);

    bool holdsLock(Transaction);

};

#endif // ITEM_H

#ifndef LOCK_H
#define LOCK_H

#include "transaction.h"

class Lock
{
private:
    int lockState;
    vector<Transaction> lockingTransaction;
public:
    Lock();

    void setLockState(int);
    int getLockState();

    int getNumberOfTransactions();

    void addLockingTransaction(Transaction);
    bool removeLockingTransaction(Transaction);
    Transaction getLockingTransaction(int);
};

#endif // LOCK_H

#ifndef WAITQ_H
#define WAITQ_H

#include "transaction.h"

class WaitQ
{
private:
    vector<Transaction> transactions;
    vector<int> lockTypes;

public:
    WaitQ();
    void insert(Transaction, int);
    void remove(Transaction);
    Transaction getNextTransaction();
    int getNextLockType();
    bool isEmpty();
};

#endif // WAITQ_H

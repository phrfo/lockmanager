#ifndef TRANSACTION_H
#define TRANSACTION_H

#include "general.h"

class Transaction
{
private:
    string name;
    int timestamp;
    vector <string> currentLocks;

public:
    Transaction();
    Transaction(string, int);
    string getName();
    void setName(string);
    int getTimestamp();
    void setTimestamp(int TS);
    void addLock(string);
    void removeLock(string);
    int getNumberCurrentLocks();
    string getLockedId(int);
};

#endif // TRANSACTION_H

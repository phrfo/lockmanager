#ifndef LOCKTABLE_H
#define LOCKTABLE_H

#include "item.h"

class LockTable
{
private:

public:
    vector <Item> itemList;
    LockTable();
    void addItem(Item newItem);
    Item* getItem(string id);
    void updateItem(Item);
};

#endif // LOCKTABLE_H

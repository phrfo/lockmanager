#include "item.h"

Item::Item(string name) {
    id = name;
}

string Item::getId() {
    return id;
}

Lock Item::getLock()
{
    return lock;
}

bool Item::isLocked() {
    if (lock.getLockState() == 0)
        return false;
    else
        return true;
}

bool Item::isLockedForRead() {
    if (lock.getLockState() > 1)
        return true;
    else
        return false;
}

bool Item::isLockedForWrite(Transaction t) {
    return (isLockedForRead()||(lock.getLockState()==1 && (!holdsLock(t)||(lock.getNumberOfTransactions()!=1))));
}

void Item::addReadLock(Transaction t) {
    if (!isLockedForRead()) {
        cout << "lock compartilhado do item " << getId() << " foi criado com sucesso" << endl;
//        cout << t.getTimestamp() << endl;
        lock.addLockingTransaction(t);
        lock.setLockState(1);
        t.addLock(getId());

    }
    else
    {
        cout << "Requisição de lock compartilhado do item " << getId() << " foi enfileirada" << endl;
        queue.insert(t,1);
    }

}

void Item::addWriteLock(Transaction t) {
    if (!isLockedForWrite(t)) {
        cout << "lock exclusivo do item " << getId() << " foi criado com sucesso" << endl;
//        cout << t.getTimestamp() << endl;
        lock.addLockingTransaction(t);
        lock.setLockState(2);
        t.addLock(getId());
    }
    else
    {
        cout << "Requisição de lock exclusivo do item " << getId() << " foi enfileirada" << endl;
//        cout << t.getTimestamp() << endl;
        queue.insert(t,2);
    }
}

void Item::unlock(Transaction t) {
    if (isLocked()) {
        lock.removeLockingTransaction(t);
        if (lock.getNumberOfTransactions() == 0)
        {
            if (queue.isEmpty())
                lock.setLockState(0);
            else
            {
                if (queue.getNextLockType() == 1)
                {
                    Transaction aux = queue.getNextTransaction();
                    cout << "DESENFILEIRANDO transação " << aux.getName() << " com lock compartilhado sobre " << getId() << endl;
                    lock.setLockState(0);
                    addReadLock(aux);
                }
                else
                {
                    Transaction aux = queue.getNextTransaction();
                    lock.setLockState(0);
                    cout << "DESENFILEIRANDO transação " << aux.getName() << " com lock exclusivo sobre " << getId() << endl;
                    addWriteLock(aux);
                }
            }
        }
        else if (lock.getNumberOfTransactions() == 1)
        {
            Transaction aux = queue.getNextTransaction();
            if (holdsLock(aux)) {
                lock.setLockState(0);
                cout << "DESENFILEIRANDO transação " << aux.getName() << " com lock exclusivo sobre " << getId() << endl;
                addWriteLock(aux);
            }
        }
    }
}

bool Item::isWaitQEmpty() {
    return queue.isEmpty();
}

void Item::addInWaitQ(Transaction t, int lockType) {
    switch(lockType) {
    case 1: addReadLock(t); break;
    case 2: addWriteLock(t); break;
    }
}

bool Item::holdsLock(Transaction t)
{
    bool output = false;
    for (int i=0; i<getLock().getNumberOfTransactions(); i++)
        if (lock.getLockingTransaction(i).getName() == t.getName())
            output = true;
    return output;
}

void Item::removeFromQueue(Transaction t)
{
    this->queue.remove(t);
}



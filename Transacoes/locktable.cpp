#include "locktable.h"

LockTable::LockTable()
{

}

void LockTable::addItem(Item i)
{
    itemList.push_back(i);
    cout << "item " << i.getId() << " adicionado com sucesso" << endl;
}

Item* LockTable::getItem(string id)
{
    for (unsigned int k=0; k<itemList.size(); k++)
    {
        if (itemList.at(k).getId() == id)
        {
            return &itemList.at(k);
        }
    }
    return new Item("Not Found");
}

void LockTable::updateItem(Item i)
{
    for (unsigned int k=0; k<itemList.size(); k++)
    {
        if (itemList.at(k).getId() == i.getId())
        {
            itemList.at(k) = i;
        }
    }
}


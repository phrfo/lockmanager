#include "transaction.h"


Transaction::Transaction()
{

}

Transaction::Transaction(string name,int TS)
{
    this->name = name;
    timestamp = TS;
}

string Transaction::getName()
{
    return name;
}
void Transaction::setName(string name)
{
    this->name = name;
}

int Transaction::getTimestamp() {
    return timestamp;
}

void Transaction::setTimestamp(int TS) {
    timestamp = TS;
}

void Transaction::addLock(string id)
{
    currentLocks.push_back(id);
}

void Transaction::removeLock(string id)
{
    for (int i=0; i<currentLocks.size(); i++)
    {
        if (currentLocks.at(i) == id)
        {
            currentLocks.erase(currentLocks.begin() + i);
        }
    }
}

int Transaction::getNumberCurrentLocks()
{
    return currentLocks.size();
}

string Transaction::getLockedId(int i)
{
    return currentLocks.at(i);
}

#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "general.h"
#include "state.h"
#include "lockmanager.h"

class Controller
{
private:
    State started;
    State active;
    State cancel_process;
    State execution_process;
    State executed;
    State finished;
    LockManager lockManager;
    vector <Transaction> transactions;
public:
    int number_of_transactions;
    Controller();
    void begin(string name, int timestamp);
    void read(Transaction, string);
    void write(Transaction, string);
    void terminate(Transaction);
    void rollback(Transaction);
    void commit(Transaction);
    void finish(Transaction);
    void print_states();
    Transaction findTransactionByName(string);
};

#endif // CONTROLLER_H

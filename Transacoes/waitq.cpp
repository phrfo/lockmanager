#include "waitq.h"

WaitQ::WaitQ()
{

}

void WaitQ::insert(Transaction t, int i)
{
    transactions.push_back(t);
    lockTypes.push_back(i);
}



Transaction WaitQ::getNextTransaction()
{
    Transaction t;
    t = transactions.at(0);
    transactions.erase(transactions.begin());
    lockTypes.erase(lockTypes.begin());
    return t;
}

int WaitQ::getNextLockType() {
    return lockTypes.at(0);
}

bool WaitQ::isEmpty()
{
    return transactions.empty();
}

void WaitQ::remove(Transaction t)
{
    for (int i=0; i<transactions.size(); i++)
    {
        if (transactions.at(i).getName() == t.getName())
        {
            transactions.erase(transactions.begin() + i);
            lockTypes.erase(lockTypes.begin() + i);
            i--;
        }
    }
}

#ifndef LOCKMANAGER_H
#define LOCKMANAGER_H

#include "locktable.h"

class Controller;

class LockManager
{
private:
    Controller* c;
public:
    LockTable listOfItems;
    LockManager();
    LockManager(Controller*);
    void LS(Transaction, string);
    void LX(Transaction, string);
    void U(Transaction, string);
};

#endif // LOCKMANAGER_H

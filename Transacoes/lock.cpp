#include "lock.h"

Lock::Lock()
{
    lockState = 0;
}

void Lock::setLockState(int state)
{
    lockState = state;
}

int Lock::getLockState()
{
    return lockState;
}

int Lock::getNumberOfTransactions()
{
    return lockingTransaction.size();
}

void Lock::addLockingTransaction(Transaction t)
{
    lockingTransaction.push_back(t);
}

bool Lock::removeLockingTransaction(Transaction t)
{
    int pos = -1;
    for (int i=0; i<lockingTransaction.size(); i++)
    {
        if (lockingTransaction.at(i).getName() == t.getName())
        {
            pos = i;
        }
    }
    if (pos == -1) {
        return false;
    }
    else {
        lockingTransaction.erase(lockingTransaction.begin() + pos);
        return true;
    }
}

Transaction Lock::getLockingTransaction(int i)
{
    return lockingTransaction.at(i);
}

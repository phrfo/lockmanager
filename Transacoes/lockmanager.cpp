#include "lockmanager.h"
#include "controller.h"

LockManager::LockManager()
{

}

LockManager::LockManager(Controller *refc)
{
    c = refc;
}

void LockManager::LS(Transaction t, string id)
{
    if (listOfItems.getItem(id)->getId() == "Not Found")
    {
        listOfItems.addItem(id);
    }


    Item *i = listOfItems.getItem(id);
    if (!i->holdsLock(t))
    {
        cout << "A transação " << t.getName() << " requisitou um bloqueio compartilhado do item " << id << endl;
        i->addReadLock(t);
    }
    else
    {
        cout << "A transação " << t.getName() << " já possui um bloqueio do item " << id << endl;
    }

}

void LockManager::LX(Transaction t, string id)
{
    if (listOfItems.getItem(id)->getId() == "Not Found")
    {
        listOfItems.addItem(id);
    }
    Item *i = listOfItems.getItem(id);
    if (i->getLock().getLockState()==2 && i->holdsLock(t))
        cout << "A transação " << t.getName() << " já possui um bloqueio exclusivo do item " << id << endl;
    else
    {
        cout << "A transação " << t.getName() << " requisitou um bloqueio exclusivo do item " << id << endl;
        for (int k=0; k<i->getLock().getNumberOfTransactions(); k++)
        {
            cout << i->getLock().getNumberOfTransactions() << endl;
            if (i->getLock().getLockingTransaction(k).getTimestamp() > t.getTimestamp())
            {
                cout << "Deu Rollback" << endl;
                c->rollback(i->getLock().getLockingTransaction(k));
                k--;
            }
        }
        i->addWriteLock(t);
    }
}

void LockManager::U(Transaction t, string id)
{
    Item *i = listOfItems.getItem(id);
    if (i->holdsLock(t))
    {
        cout << "A transação " << t.getName() << " desbloqueou o item " << id << endl;
        i->unlock(t);
    }
}

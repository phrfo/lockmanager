#include "general.h"
#include "state.h"
#include "transaction.h"
#include "controller.h"

int menu() {
    int opc;
    cout << "MENU - Digite a opção desejada:" << endl;
    cout << "1 - TR_Begin" << endl;
    cout << "2 - Read" << endl;
    cout << "3 - Write" << endl;
    cout << "4 - TR_Terminate" << endl;
    cout << "5 - TR_Rollback" << endl;
    cout << "6 - TR_Commit" << endl;
    cout << "7 - TR_Finish" << endl;
    cout << "8 - Sair" << endl;
    cin >> opc;
    return opc;
}

int main()
{
    int opc;
    Controller c;
    string name;
    string idItem;
    Transaction t;
    while (opc != 8)
    {
        //system("clear");
        cout << endl << endl;
        c.print_states();
        opc = menu();

        if (opc == 1) {
            cout << "Digite o nome da nova transacao" << endl;
            cin >> name;
            c.begin(name, c.number_of_transactions);
        }
        else if (opc != 8){
            cout << "Digite o nome da transacao" << endl;
            cin >> name;
            t = c.findTransactionByName(name);
            switch(opc) {
            case 2:
                cout << "entre com o id do item desejado" << endl;
                cin >> idItem;
                c.read(t,idItem);
            break;
            case 3:
                cout << "entre com o id do item desejado" << endl;
                cin >> idItem;
                c.write(t,idItem);
            break;
            case 4: c.terminate(t); break;
            case 5: c.rollback(t); break;
            case 6: c.commit(t); break;
            case 7: c.finish(t); break;
            }
        }
    }
}
